<?php
/**
 * 1.
 *
 * Наследование - это заимствование свойств и функционала от имеющегося класса в новом. Т.е. при наследовании от класса
 * Figure, имеющего публичные или защищенные поля sizeX, sizeY, color, а так же методы draw, rotate и пр., все эти
 * члены класса будут доступны и в наследнике. В наследнике можно переопределить поля и методы, изменив поведение
 * нового объекта.
 *
 * Полиморфизм - это возможность использования экземпляров классов наследников вместо экземпляра родительского класса.
 * Если мы определили класс Box, наследника класса Figure, переопределили в нем методы draw и rotate на свои, а
 * так же реализовали новые методы, то мы можем обращаться к экземпляру этого класса, через интерфейс более общего
 * класса, класса родителя (Figure) при необходимости. При этом нам будет доступна функциональность класса на
 * уровне класса-родителя (доступны описанные в классе Figure члены).
 *
 * 2.
 *
 * Отличие интерфейсов и абстрактных классов.
 *
 * Абстрактные классы - это классы с частично реализованной функциональностью. Создавать экземпляры абстрактных классов
 * не возможно, можно лишь наследоваться от абстрактных классов, при этом необходимо переопределить абстрактные методы.
 *
 * Интерфейсы - это описание (без реализации) необходимых членов, которые должны иметься в классе, реализующем данный
 * интерфейс.
 *
 * Отличие состоит в том, что разрешено наследоваться лишь от одного класса, но можно реализовать в классе множество
 * интерфейсов, а так же, абстрактные класса могут содержать реализацию каких-либо методов, а интерфейсы могут лишь
 * описать их сигнатуру.
 *
 * 3.
 *
 */


//Супер класс для объектов из прошлого ДЗ
abstract class Object
{
    // Модель, имя или название
    protected $title;
    // Цвет
    protected $color;

    public function __construct($title, $color)
    {
        $this->title = $title;
        $this->color = $color;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->name = $title;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }
}

// 4.

/**
 * Interface CarInterface
 */

interface CarInterface {
    public function getSpeed();
    public function setSpeed($speed);
    public function go();
    public function turnLeft();
    public function turnRigth();
}

/**
 * Class Car
 */

class Car extends Object implements CarInterface {
    protected $speed;

    /**
     * Car constructor.
     * @param $title
     * @param $color
     * @param $speed
     */
    function __construct($title, $color, $speed)
    {
        parent::__construct($title, $color);
        $this->speed = $speed;
    }

    public function getSpeed()
    {
        return $this->speed;
    }

    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    public function go()
    {
        echo $this->title . ' поехал';
    }

    public function turnLeft()
    {
        echo $this->title . ' повернул влево';
    }

    public function turnRigth()
    {
        echo $this->title . ' повернул направо';
    }
}

/**
 * Interface TelevisionInterface
 */

interface TelevisionInterface {
    public function getDiagonal();
    public function setDiagonal($diagonal);
    public function getDisplayType();
    public function setDisplayType($displayType);
    public function turnOn($on);
    public function getState();
}

class Television extends Object implements TelevisionInterface {
    protected $diagonal;
    protected $displayType;
    protected $state;

    /**
     * Television constructor.
     * @param $title
     * @param $color
     * @param $diagonal
     * @param $displayType
     * @internal param $state
     */
    public function __construct($title, $color, $diagonal, $displayType)
    {
        parent::__construct($title, $color);
        $this->diagonal = $diagonal;
        $this->displayType = $displayType;
    }

    public function getDiagonal()
    {
        return $this->diagonal;
    }

    public function setDiagonal($diagonal)
    {
        $this->diagonal = $diagonal;
    }

    public function getDisplayType()
    {
        return $this->displayType;
    }

    public function setDisplayType($displayType)
    {
        $this->displayType = $displayType;
    }

    public function turnOn($on)
    {
        $this->state = $on;
        echo $this->title . ' ' . $on ? 'ВКЛ' : 'ВЫКЛ';
    }

    public function getState()
    {
        return $this->state;
    }

}

/**
 * Interface DuckInterface
 */
interface DuckInterface {
    public function fly();
    public function go();
    public function swim();
}

/**
 * Class Duck
 */
class Duck extends Object implements DuckInterface {
    public function fly()
    {
        echo $this->title . ' летит';
    }

    public function go()
    {
        echo $this->title . ' идет';
    }

    public function swim()
    {
        echo $this->title . ' плывет';
    }
}

/**
 * Interface BallPenInterface
 */
interface BallPenInterface {
    public function write();
}

/**
 * Class BallPen
 */
class BallPen extends Object implements BallPenInterface {
    protected $thin;

    public function __construct($title, $color, $thin)
    {
        parent::__construct($title, $color);
        $this->thin = $thin;
    }

    public function write()
    {
        echo 'Ручка пишет, цвет ' . $this->color . ', толщина ' . $this->thin;
    }
}

/**
 * Interface ProductInterface
 */

interface ProductInterface {
    public function getPrice();
    public function setDiscount($discount);
}

/**
 * Class Product
 */

class Product extends Object implements ProductInterface {

    protected $price;
    protected $discount;

    public function __construct($title, $price, $discount)
    {
        $this->title = $title;
        $this->price = $price;
        $this->discount = $discount;
    }

    public function getPrice()
    {
        return $this->price - $this->price / 100 * $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

}